package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;

class ThreadControllerTest {

    private Thread firstThread;
    private Thread secondThread;
    private ThreadController threadController;
    private Post post;

    @BeforeEach
    void init() {
        firstThread = new Thread("Dariya", new Timestamp(10L), "forum", "message", "slug", "title", 10);
        secondThread =
                new Thread("Dariya 2", new Timestamp(100L), "forum  2", "message 2", "slug 2", "title 2", 100);
        firstThread.setId(1);
        secondThread.setId(2);
        post = new Post();
        threadController = new ThreadController();
    }


    @Test
    @DisplayName("Test create post")
    void testCreatePost() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(firstThread);
                ResponseEntity result = threadController.createPost("slug", List.of(post));
                assertEquals(HttpStatus.CREATED, result.getStatusCode());
                assertEquals(result.getBody(), List.of(post));
            }
        }
    }

    @Test
    @DisplayName("Test get posts")
    void testPosts() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(firstThread);
                threadDao.when(() -> ThreadDAO.getPosts(any(), any(), any(), any(), any()))
                        .thenReturn(List.of(post));
                ResponseEntity result = threadController.Posts("slug", 1, 2, "asc", true);
                assertEquals(HttpStatus.OK, result.getStatusCode());
                assertEquals(result.getBody(), List.of(post));
            }
        }
    }

    @Test
    @DisplayName("Test change info")
    void testChange() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(firstThread);
                threadDao.when(() -> ThreadDAO.getThreadById(anyInt())).thenReturn(secondThread);
                ResponseEntity result = threadController.change("slug", secondThread);
                assertEquals(HttpStatus.OK, result.getStatusCode());
                assertEquals(result.getBody(), secondThread);
            }
        }
    }

    @Test
    @DisplayName("Test get thread info")
    void testInfo() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(firstThread);
                ResponseEntity result = threadController.info("slug");
                assertEquals(HttpStatus.OK, result.getStatusCode());
                assertEquals(result.getBody(), firstThread);
            }
        }
    }

    @Test
    @DisplayName("Test create vote")
    void testCreateVote() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Info(any())).thenReturn(new User());
            try (MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class)) {
                threadDao.when(() -> ThreadDAO.getThreadBySlug(anyString())).thenReturn(firstThread);
                threadDao.when(() -> ThreadDAO.change(any(), anyInt())).thenReturn(10);
                int voteBefore = firstThread.getVotes();
                ResponseEntity result = threadController.createVote("slug", new Vote("Dariya", 100));
                assertEquals(HttpStatus.OK, result.getStatusCode());
                assertEquals(result.getBody(), firstThread);
                int voteAfter = firstThread.getVotes();
                assertEquals(voteAfter, voteBefore + 100);
            }
        }
    }
}